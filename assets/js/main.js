$(document).ready(function(){
    ////////////////////////////////////////////
    function move_desk_mobile(item, mobile, mq){
        if ( window.innerWidth  <= mq && !$(item).hasClass('move-changed')) {
            $(mobile).append($(item));
            $(item).addClass('move-changed');
        }
    }
    
    ////////////////////////////////////////////
    function back_mobile_desktop(item, desktop, mq){
        if ( window.innerWidth  > mq && $(item).hasClass('move-changed')) {
            $(desktop).append($(item));
            $(item).removeClass('move-changed');
        }
    }

    ////////////////////////////////////////////
    function custom_input_number(){
        $('.number-count').each(function(index){
            $(this).find('.increase-value').click(function(event) {
                event.preventDefault();
                var input_el=$(this).parents('.number-count').children('input');
                var v= input_el.val()*1+1;
                if(v<=input_el.attr('max'))
                input_el.val(v)
            });
            $(this).find('.decrease-value').click(function(event) {
                event.preventDefault();
                var input_el=$(this).parents('.number-count').children('input');
                var v= input_el.val()-1;
                if(v>=input_el.attr('min'))
                input_el.val(v)
            });
        });
    }

    ////////////////////////////////////////////
    function carousel_mobile(){
        if ( window.innerWidth  <= 639) {
            $('#landing_main .info-content .carousel-container').slick({
                dots: true,
                arrows: false,
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 3000,
            });
        }
    }
    function smooth_scroll(){
        var easeInOutQuad = new SmoothScroll('[data-easing="easeInOutQuad"]', {easing: 'easeInOutQuad'});
    }
    function select_custom(){
        $('.select-group .custom-select').each(function(index){
            //console.log('se ejecuto el N ' + Number(index + 1) );
            var $this = $(this), numberOfOptions = $(this).children('option').length;

            $this.addClass('select-hidden'); 
            $this.wrap('<div class="select"></div>');
            $this.after('<div class="select-styled"></div>');
        
            var $styledSelect = $this.next('div.select-styled');
            $styledSelect.text($this.children('option').eq(0).text());
            
            var $list = $('<ul />', {
                'class': 'select-options'
            }).insertAfter($styledSelect);
            
            for (var i = 0; i < numberOfOptions; i++) {
                $('<li />', {
                    text: $this.children('option').eq(i).text(),
                    rel: $this.children('option').eq(i).val()
                }).appendTo($list);
            }
            
            var $listItems = $list.children('li');
            
            $styledSelect.click(function(e) {
                e.stopPropagation();
                $('div.select-styled.active').not(this).each(function(){
                    $(this).removeClass('active').next('ul.select-options').hide();
                });
                $(this).toggleClass('active').next('ul.select-options').toggle();
            });
            
            $listItems.click(function(e) {
                e.stopPropagation();
                $styledSelect.text($(this).text()).removeClass('active');
                $this.val($(this).attr('rel'));
                $list.hide();
                //console.log($this.val());
            });
            
            $(document).click(function() {
                $styledSelect.removeClass('active');
                $list.hide();
            });
        
        });
    }
    
    ////////////////////////////////////////////
    carousel_mobile();
    smooth_scroll();
    select_custom();
    //EJECUCION DE FUNCIONES (Rezize)
    window.addEventListener('resize', function () {
    
    });
});

////////////////////////////////////////////
$( window ).on( "load", function(){
        
}); 



